package main 	

import (
    "fmt"
    "io/ioutil"
    "math/cmplx"
    "os"
    "math"
    "encoding/binary"
)


var ( 
	px,w,h	float64
	x,y	int
	p,q	[]byte
	perms	os.FileMode
	c,z	complex128
)

func main() {
	w,h = 100.0,100.0
	x,y = 0,0

	//Convert the magic string to a byte slice
	magic := []byte("P5\n100 100\n255\n")
	

	for i := 0.0; i < h; i++ {
		for j := 0.0; j < w; j++ {
			c = complex(-2.0 + j*4.0/w, 2.0 - i*4.0/h)
			tmp := mandelbrot(c)
			byte := float64ToByte(tmp)
			p = append(p, byte...)
		}
	}
	
	fmt.Println(p)

	q = append(magic, p...)
	perms = 0644
	ioutil.WriteFile("mandelbrot_go.pgm",q,perms)
}

func mandelbrot(c complex128) float64 {
	z = complex(0,0)
	px = 255

	for {
		z = z*z + c
		if cmplx.Abs(z) > 4.0 { return px } else if px < 11 { return px }
		px = px - 10
			
	}
}

func float64ToByte(f float64) []byte {
   var buf [8]byte
   binary.BigEndian.PutUint64(buf[:], math.Float64bits(f))
   return buf[:]
}

