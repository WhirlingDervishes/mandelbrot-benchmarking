#with appreciation to Ted Burke
#https://batchloaf.wordpress.com/2016/02/03/c-or-python-comparison-of-execution-time-for-mandelbrot-image-generation/


import numpy

def mandelbrot(c):
	z = complex(0,0)	
	#px is the pgm representation of the shade of the pixel at p[x][y]
	px = 255
	while abs(z) <= 4.0 and px >= 10:
		z = z*z + c
		px -= 10
	return px


#Initialize the array that will become the PGM bytearray
w,h = 1000,1000
p = numpy.zeros((h,w),dtype=numpy.uint8)


#create file for pgm, write the magic number so the file will be valid
#http://netpbm.sourceforge.net/doc/pgm.html
#Later, we will write the array p as a bytearray so the magic number string needs to be byte encoded first
f = open('mandelbrot_py.pgm', 'wb')
f.write(('P5\n{:d} {:d}\n255\n'.format(w, h)).encode())


for y in range(h):
	for x in range(w):
		c = complex(-2.0 + x*4.0/w,2.0 - y*4.0/h)
		#print(c, mandelbrot(c))
		p[y][x] = mandelbrot(c)
print(p)
f.write(bytearray(p))
f.close()
