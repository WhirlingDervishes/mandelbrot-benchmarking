# Benchmarking Various Languages with Fractals

Code used to benchmark how fast various programming languages can generate a 1000x1000 grayscale image of a Mandelbrot set.

Results available at [https://whirlingdervish.es](https://whirlingdevish.es/posts/0121/benching.html)

In case the elapsed times seem inflated to you, be aware that my tests were conducted on my trusty Dell Lattitude D410 from 2005.

[Specs from CNET](https://www.cnet.com/products/dell-latitude-d410/specs/):

### PROCESSOR / CHIPSET
CPU Intel Pentium M 755 / 2 GHz
Cache L2 - 2 MB
Data Bus Speed 400 MHz
Chipset Type Mobile Intel 915GM Express

### CACHE MEMORY
Type L2 cache
Installed Size 2 MB

### RAM
Technology DDR2 SDRAM
Installed Size 512 MB

